#include <osclooper/loop.hpp>
#include <osclooper/exceptions.hpp>

#include <cppunit/extensions/HelperMacros.h>
#include <vector>
#include <stdexcept>
#include <chrono>
using namespace std::literals::chrono_literals;

class loopTest : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(loopTest);
  CPPUNIT_TEST(testDefaultConstructor);
  CPPUNIT_TEST_SUITE_END();
 public:
  void setUp();
  void tearDown();
  void testDefaultConstructor();
};

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(loopTest);

void loopTest::setUp() {}

void loopTest::tearDown() {}

void loopTest::testDefaultConstructor()
{
  CPPUNIT_ASSERT_THROW(loop test_loop,
                       NotImplemented);
}

#include "test_common.hpp"
