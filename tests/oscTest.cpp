#include <osclooper/osc.hpp>
#include <osclooper/exceptions.hpp>

#include <cppunit/extensions/HelperMacros.h>
#include <vector>
#include <stdexcept>
#include <chrono>
#include <thread>
using namespace std::literals::chrono_literals;

class oscTest : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(oscTest);
  CPPUNIT_TEST(testNode_DefaultConstructor);
  CPPUNIT_TEST(testNode_set_parent);
  CPPUNIT_TEST(testNode_get_root_parent);
  CPPUNIT_TEST(testNode_get_children);
  CPPUNIT_TEST(testNode_control_handler_exception);
  CPPUNIT_TEST(testNode_add_osc_method);

  CPPUNIT_TEST(testRootNode_DefaultConstructor);
  CPPUNIT_TEST(testRootNode_ConflictedOSCPort);
  CPPUNIT_TEST(testRootNode_get_osc_path);
  CPPUNIT_TEST_SUITE_END();
 public:
  bool ok;
  void setUp();
  void tearDown();
  void testNode_DefaultConstructor();
  void testNode_set_parent();
  void testNode_get_root_parent();
  void testNode_get_children();
  void testNode_control_handler_exception();
  void testNode_add_osc_method();

  void testRootNode_DefaultConstructor();
  void testRootNode_ConflictedOSCPort();
  void testRootNode_get_osc_path();
 private:
  osc::RootNode *test_root_node;
  osc::Node *test_child_node;
};

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(oscTest);

void oscTest::setUp() {
  ok = false;
}

void oscTest::tearDown() {
  delete test_root_node;
  delete test_child_node;
}

/******************************
 ** osc::Node *****************
 ******************************/
void oscTest::testNode_DefaultConstructor()
{
  osc::Node my_osc_node("test_node_name", nullptr);
  CPPUNIT_ASSERT(nullptr == my_osc_node.get_parent());
  CPPUNIT_ASSERT_EQUAL(my_osc_node.get_name(),
                       std::string("test_node_name"));
  test_child_node = new osc::Node("test_node_name_pointer", &my_osc_node);
  CPPUNIT_ASSERT_EQUAL(&my_osc_node,
                       test_child_node->get_parent());
  CPPUNIT_ASSERT_EQUAL(test_child_node->get_name(),
                       std::string("test_node_name_pointer"));
}

void oscTest::testNode_set_parent()
{
  test_root_node = new osc::RootNode("root_node");
  CPPUNIT_ASSERT(nullptr == test_root_node->get_parent());
  test_child_node = new osc::Node("child_node", nullptr);
  CPPUNIT_ASSERT(nullptr == test_child_node->get_parent());
  test_child_node->set_parent(test_root_node);
  CPPUNIT_ASSERT(test_root_node == test_child_node->get_parent());
  test_child_node->set_parent(nullptr);
  CPPUNIT_ASSERT(test_root_node == test_child_node->get_parent());
}

void oscTest::testNode_get_root_parent()
{
  test_root_node = new osc::RootNode("root_node");
  CPPUNIT_ASSERT(nullptr == test_root_node->get_parent());
  test_child_node = new osc::Node("child_node", nullptr);
  CPPUNIT_ASSERT(nullptr == test_child_node->get_parent());
  test_child_node->set_parent(test_root_node);
  CPPUNIT_ASSERT(test_root_node == test_child_node->get_parent());
  osc::Node test_grand_child_node("grand_child", test_child_node);
  CPPUNIT_ASSERT(test_root_node == test_grand_child_node.get_root_parent());
}

void oscTest::testNode_get_children()
{
  osc::Node parent_node("test_node_parent", nullptr);
  CPPUNIT_ASSERT(nullptr == parent_node.get_parent());
  CPPUNIT_ASSERT_EQUAL(parent_node.get_name(),
                       std::string("test_node_parent"));
  test_child_node = new osc::Node("test_child_node", &parent_node);
  CPPUNIT_ASSERT(parent_node.get_children());
  CPPUNIT_ASSERT_EQUAL(static_cast<std::size_t>(1),
                       parent_node.get_children()->size());
  CPPUNIT_ASSERT(parent_node.get_children()->front()
                 == test_child_node);
}

void oscTest::testNode_control_handler_exception()
{
  test_child_node = new osc::Node("child_node", nullptr);
  CPPUNIT_ASSERT_THROW(test_child_node->control_handler("", "", nullptr, 0, nullptr, this),
                       NotImplemented);
}

static int testNode_control_handler(const char *path,
                            const char *types,
                            lo_arg **arguments,
                            int nbarguments,
                            void *,//message,
                            void *user_data)
{
  oscTest *data = static_cast<oscTest *>(user_data);
  std::string method(std::string(path).substr(std::string(path).rfind("/")));
  std::string arguments_types(types);
  CPPUNIT_ASSERT_EQUAL(std::string("/test_it"),
                       method);
  CPPUNIT_ASSERT_EQUAL(std::string("ss"),
                       arguments_types);
  CPPUNIT_ASSERT_EQUAL(2,
                       nbarguments);
  CPPUNIT_ASSERT_EQUAL(std::string("one"),
                       std::string(&arguments[0]->s));
  CPPUNIT_ASSERT_EQUAL(std::string("two"),
                       std::string(&arguments[1]->s));
  data->ok = true;
  return 0;
}

void oscTest::testNode_add_osc_method()
{
  test_root_node = new osc::RootNode("root_node", 9123);
  test_child_node = new osc::Node("child_node", test_root_node);
  test_child_node->add_osc_method("/test_it", "ss",
                                  testNode_control_handler,
                                  this);
  test_root_node->start();
  lo::Address address("localhost", 9123);
  address.send("/test_it", "ss", "one", "two");
  std::this_thread::sleep_for(100ms);
  CPPUNIT_ASSERT(this->ok);  // its value must have been changed by
                             // the handler callback
}

/******************************
 ** osc::RootNode *************
 ******************************/
void oscTest::testRootNode_DefaultConstructor()
{
  osc::RootNode my_osc_root_node("test_root_node_name");
  CPPUNIT_ASSERT(nullptr == my_osc_root_node.get_parent());
  CPPUNIT_ASSERT_EQUAL(my_osc_root_node.get_name(),
                       std::string("test_root_node_name"));
}

void oscTest::testRootNode_ConflictedOSCPort()
{
  std::string _name = "testRootNodeConflictedOSCPort";
  std::string _other_name = "testRootNodeConflictedOSCPort_otherName";
  test_root_node = new osc::RootNode(_name);
  CPPUNIT_ASSERT_THROW(new osc::RootNode(_other_name),
                       std::invalid_argument);
}

void oscTest::testRootNode_get_osc_path()
{
  std::string _root_name = "testRootNode";
  std::string _child_name = "testChildNode";
  std::string _grand_child_name = "testGrandChildNode";
  test_root_node = new osc::RootNode(_root_name);
  test_child_node = new osc::Node(_child_name, test_root_node);
  osc::Node *grand_child_node = new osc::Node(_grand_child_name, test_child_node);
  CPPUNIT_ASSERT_EQUAL("/"+_root_name+"/"+_child_name,
                       test_child_node->get_osc_path());
  CPPUNIT_ASSERT_EQUAL("/"+_root_name+"/"+_child_name+"/"+_grand_child_name,
                       grand_child_node->get_osc_path());
  delete grand_child_node;
}

#include "test_common.hpp"
