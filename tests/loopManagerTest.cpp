#include <osclooper/loopManager.hpp>

#include <cppunit/extensions/HelperMacros.h>
#include <vector>
#include <stdexcept>
#include <chrono>
using namespace std::literals::chrono_literals;

class loopManagerTest : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(loopManagerTest);
  CPPUNIT_TEST(testDefaultConstructor);
  CPPUNIT_TEST_SUITE_END();
 public:
  void setUp();
  void tearDown();
  void testDefaultConstructor();
};

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(loopManagerTest);

void loopManagerTest::setUp() {}

void loopManagerTest::tearDown() {}

void loopManagerTest::testDefaultConstructor()
{
  loopManager test_loop_manager;
  CPPUNIT_ASSERT(0 == test_loop_manager.get_loops()->size());
}

#include "test_common.hpp"
