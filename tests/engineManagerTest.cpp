#include <osclooper/engineManager.hpp>

#include <cppunit/extensions/HelperMacros.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <stdexcept>
#include <chrono>
using namespace std::literals::chrono_literals;

class engineManagerTest : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(engineManagerTest);
  CPPUNIT_TEST(testSingleton);
  CPPUNIT_TEST(testEmptyEnginesAfterFreshlyInstanced);
  CPPUNIT_TEST(testAddEngine);
  CPPUNIT_TEST(testAddEngineWithSameName);
  CPPUNIT_TEST(testAddEngineWithSamePort);
  CPPUNIT_TEST(testRegistryMaxSize);
  CPPUNIT_TEST(testListing);
  CPPUNIT_TEST_SUITE_END();
 public:
  void setUp();
  void tearDown();
  void testSingleton();
  void testEmptyEnginesAfterFreshlyInstanced();
  void testAddEngine();
  void testAddEngineWithSameName();
  void testAddEngineWithSamePort();
  void testRegistryMaxSize();
  void testListing();
 private:
  std::ostringstream local;
  std::streambuf *cout_buff;
};

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(engineManagerTest);

void engineManagerTest::setUp()
{
  cout_buff = std::cout.rdbuf();  ///> save pointer to std::cout buffer
  std::cout.rdbuf(local.rdbuf());  ///> set new local buffer to std::cout buffer
}

void engineManagerTest::tearDown()
{
  std::cout.rdbuf(cout_buff);  ///> go back to old buffer
  delete(engineManager::instance());
}

void engineManagerTest::testSingleton()
{
  engineManager *test_engineManager, *test_other_engineManager;
  test_engineManager = engineManager::instance();
  test_other_engineManager = engineManager::instance();
  CPPUNIT_ASSERT(test_engineManager == test_other_engineManager);
}

void engineManagerTest::testEmptyEnginesAfterFreshlyInstanced()
{
  engineManager *test_engineManager;
  test_engineManager = engineManager::instance();
  CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(0),
                       test_engineManager->get_engines()->size());
}

void engineManagerTest::testAddEngine()
{
  engineManager *test_engineManager;
  test_engineManager = engineManager::instance();
  test_engineManager->add("testAddEngine", 10ms, 9876);
  CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(1),
                       test_engineManager->get_engines()->size());
  CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(1),
                       test_engineManager->get_engines()->count("testAddEngine"));
  CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(0),
                       test_engineManager->get_engines()->count("invalidEngineName"));
}

void engineManagerTest::testAddEngineWithSameName()
{
  engineManager *test_engineManager;
  test_engineManager = engineManager::instance();
  test_engineManager->add("firstEngine", 10ms, 9876);
  CPPUNIT_ASSERT_THROW(test_engineManager->add("firstEngine", 1ms, 9877),
                       std::invalid_argument);
  CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(1),
                       test_engineManager->get_engines()->size());
  CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(1),
                       test_engineManager->get_engines()->count("firstEngine"));
  CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(0),
                       test_engineManager->get_engines()->count("secondEngine"));
}

void engineManagerTest::testAddEngineWithSamePort()
{
  engineManager *test_engineManager;
  test_engineManager = engineManager::instance();
  test_engineManager->add("firstEngine", 10ms, 9876);
  CPPUNIT_ASSERT_THROW(test_engineManager->add("secondEngine", 1ms, 9876),
                       std::invalid_argument);
  CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(1),
                       test_engineManager->get_engines()->size());
  CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(1),
                       test_engineManager->get_engines()->count("firstEngine"));
  CPPUNIT_ASSERT_EQUAL(static_cast<size_t>(0),
                       test_engineManager->get_engines()->count("secondEngine"));
}

void engineManagerTest::testRegistryMaxSize()
{
  engineManager *test_engineManager;
  test_engineManager = engineManager::instance();

  for(unsigned int i = 0; i < MAX_ENGINES; ++i)
  {
    std::ostringstream compteur;
    compteur << "engine-" << i;
    test_engineManager->add(compteur.str(), 10ms, 9876 + i);
  }

  CPPUNIT_ASSERT_THROW(test_engineManager->add("one_more_is_too_much", 1s),
                       std::length_error);
}

void engineManagerTest::testListing()
{
  engineManager *test_engineManager;
  test_engineManager = engineManager::instance();
  test_engineManager->add("testAddEngine", 10ms);
  test_engineManager->list();
  CPPUNIT_ASSERT("Registered engines:\n  testAddEngine\n" == local.str());
}

#include "test_common.hpp"
