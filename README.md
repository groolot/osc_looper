# Debian like
## Pre requisites

```
apt-get update
apt-get install -y build-essential autoconf autoconf-archive automake libtool pkg-config gcovr liblo-dev liblog4cxx-dev libcppunit-dev doxygen plantuml
```

## Configure

```
autoreconf --install
./configure --enable-coverage --enable-doc
```

## Make

```
make coverage
```

## Install

```
make install
```
