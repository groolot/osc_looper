#include <osclooper/engine.hpp>
#include <osclooper/exceptions.hpp>

#include <thread>
#include <sstream>
#include <stdexcept>
#include <iomanip>
#include <utility>

using namespace std::literals::chrono_literals;

log4cxx::LoggerPtr engine::logger = log4cxx::Logger::getLogger("osclooper.engine");
std::mutex engine::latch;
std::condition_variable engine::condition;

engine::engine(std::string _name,
               std::chrono::microseconds _period,
               unsigned short int _osc_port) :
  osc::RootNode(_name, _osc_port),
  loop_manager(new loopManager()),
  period(_period),
  running_thread(false)
{
  if(!log4cxx::LogManager::getLoggerRepository()->isConfigured())
  {
    log4cxx::BasicConfigurator::configure();
  }

  control_methods = {
    {"/ping", "ss"},
    {"/start", ""},
    {"/start", "T"},
    {"/start", "F"},
    {"/stop", ""},
    {"/stop", "T"},
    {"/stop", "F"},
    {"/new_loop", "si"},
    {"/new_loop", "sii"},
    {"/remove_loop", "s"},
  };
  register_methods(control_handler, this);
  start();
}

engine::~engine()
{
  std::ostringstream output;
  stop();
  output << std::quoted(get_name())
         << " engine has been destructed";
  logger->debug(output.str());
}

void engine::set_name(std::string _name)
{
  if(! _name.empty()){
    osc::Node::set_name(_name);
    register_methods(control_handler, this);
  }
}

std::chrono::microseconds engine::get_period()
{
  return period;
}

loopManager *engine::get_loop_manager()
{
  return loop_manager;
}

unsigned short int engine::get_osc_port()
{
  return osc_port;
}

bool engine::is_running()
{
  std::lock_guard<std::mutex> locker(latch);
  return running_thread;
}

void engine::start()
{
  std::ostringstream output;
  running_thread = true;

  if(thread.joinable() == false)
  {
    thread = std::thread(&engine::run, this);
    output << std::quoted(get_name())
           << " engine started";
  }
  else
  {
    output << std::quoted(get_name())
           << " engine is ALREADY started";
  }

  condition.notify_one();
  logger->debug(output.str());
}

void engine::stop()
{
  {
    std::lock_guard<std::mutex> locker(latch);
    running_thread = false;
  }
  condition.notify_all();
  std::ostringstream output;

  if(thread.joinable())
  {
    thread.join();
    output << std::quoted(get_name())
           << " engine stopped";
  }
  else
  {
    output << std::quoted(get_name())
           << " engine ALREADY stopped";
  }

  logger->debug(output.str());
}

void engine::run()
{
  // TODO:
  //   * listen to incoming messages
  //   * analyse message for looping compliance (be sure that the message is not a future one, for example)
  //   * send message to loop manager for dispatch (recorder)
  // TODO:
  //   * set the target OSC address to the configured one
  //   * sequencely collect messages from the loop manager
  //   * send each message to the target (player)
  using clock = std::chrono::high_resolution_clock;
  std::unique_lock<std::mutex> locker(latch);

  while(running_thread)
  {
    std::ostringstream future_date, thread_id, output;
    clock::time_point future = clock::now() + period;
    execute_cycle();
    future_date << std::chrono::duration_cast<std::chrono::microseconds>(future - clock::now()).count();
    thread_id << thread.get_id();
    output << std::quoted(get_name())
           << " engine, thread #"
           << thread_id.str()
           << " tack, next in "
           << future_date.str();
    logger->trace(output.str());
    condition.wait_until(locker, future);
  }
}

void engine::execute_cycle()
{
  std::ostringstream output;
  output << "Engine name: "
         << std::quoted(get_name());
  logger->trace(output.str());
}

int engine::control_handler(const char *path,
                            const char *types,
                            lo_arg **arguments,
                            int nbarguments,
                            void *,//message,
                            void *user_data)
{
  int has_been_handled = 1;
  engine *this_engine = static_cast<engine *>(user_data);
  std::string method(std::string(path).substr(std::string(path).rfind("/")));
  std::string arguments_types(types);
  std::ostringstream output;

  // We have to discriminate within the arguments number and types
  if(method == "/ping")
  {
    if(nbarguments == 2)
    {
      unsigned int nbloops = 0;
      std::string return_url(&arguments[0]->s);
      validate_returl(return_url);
      std::string return_path(&arguments[1]->s);
      lo::Message return_message("ssi",
                                 this_engine->url().c_str(),
                                 VERSION,
                                 nbloops);
      lo::Address dest(return_url.c_str());
      dest.send(return_path.c_str(),
                return_message);
      output << this_engine->get_name()
             << "[Tx]"
             << " - " << return_path
             << " ssi @ " << return_url;
      logger->debug(output.str());
      output.str("");
      output.clear();
      has_been_handled = 0;
    }
  }
  else if(method == "/start")
  {
    if(nbarguments == 0
       || (nbarguments == 1
           && arguments_types == "T")
      )
    {
      this_engine->start();
      has_been_handled = 0;
    }
    else if(nbarguments == 1)
    {
      this_engine->stop();
      has_been_handled = 0;
    }
  }
  else if(method == "/stop")
  {
    if(nbarguments == 0
       || (nbarguments == 1
           && arguments_types == "T")
      )
    {
      this_engine->stop();
      has_been_handled = 0;
    }
    else if(nbarguments == 1)
    {
      this_engine->start();
      has_been_handled = 0;
    }
  }
  else if(method == "/new_loop")
  {
    if(nbarguments == 2)
    {
      // add new loop in the loop_manager with quantize to the lowest
      has_been_handled = 0;
    }
    else if(nbarguments == 3)
    {
      // add loop in the loop_manager with specified quantize
      has_been_handled = 0;
    }
  }
  else if(method == "/remove_loop")
  {
    if(nbarguments == 1)
    {
      // remove loop from loop_manager by name
      has_been_handled = 0;
    }
  }

  output << this_engine->get_name()
         << "[Rx]"
         << " - " << method
         << " " << types
         << " " << nbarguments
         << " @ " << this_engine->url();

  if(has_been_handled == 0)
  {
    logger->debug(output.str());
  }
  else
  {
    output << " not handled";
    logger->warn(output.str());
  }

  output.str("");
  output.clear();
  return 0;
}

void engine::validate_returl(std::string &returl)
{
  if(returl.substr(0, 10) != "osc.udp://")
  {
    returl = "osc.udp://" + returl;
  }
}
