#include <osclooper/loop.hpp>
#include <osclooper/exceptions.hpp>

using namespace std::literals::chrono_literals;

log4cxx::LoggerPtr loop::logger = log4cxx::Logger::getLogger("osclooper.loop");

loop::loop() :
  osc::Node("TODO-replace-me", nullptr)
{
  if(!log4cxx::LogManager::getLoggerRepository()->isConfigured())
  {
    log4cxx::BasicConfigurator::configure();
  }

  throw NotImplemented();
}

loop::~loop() {}

std::string loop::get_name()
{
  throw NotImplemented();
}

int loop::control_handler(const char *,//path,
                          const char *,//types,
                          lo_arg **,//arguments,
                          int ,//nbarguments,
                          void *,//message,
                          void *)//user_data)
{
  return 0;
}
