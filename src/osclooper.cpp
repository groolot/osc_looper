#include <iostream>
#include <atomic>
#include <csignal>

#include <chrono>
#include <thread>

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("osclooper"));

#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <osclooper/engineManager.hpp>

volatile std::atomic<bool> run;

using namespace std::literals::chrono_literals;

void signal_handler(int signal)
{
  logger->setLevel(log4cxx::Level::getTrace());
  std::ostringstream output;
  output << "Signal " << signal << " caugth, terminating";
  logger->info(output.str());
  run = false;
}

int main(int,// argc,
         char **argv)
{
  using hrclock = std::chrono::high_resolution_clock;
  std::cout << argv[0] << " Version " << PACKAGE_VERSION << std::endl;

  if(!log4cxx::LogManager::getLoggerRepository()->isConfigured())
  {
    log4cxx::BasicConfigurator::configure();
  }

  logger->setLevel(log4cxx::Level::getDebug());

  if(std::signal(SIGINT, signal_handler) != SIG_ERR &&
     std::signal(SIGABRT, signal_handler) != SIG_ERR &&
     std::signal(SIGTERM, signal_handler) != SIG_ERR)
  {
    hrclock::time_point next_time_point;
    run = true;
    engineManager *eManager = engineManager::instance();
    eManager->add("first_engine", 10s, 9000);
    eManager->add("second_engine", 10s, 9001);
    eManager->list();

    while(run)
    {
      next_time_point = hrclock::now() + 1ms;
      std::this_thread::sleep_until(next_time_point);
    }

    delete eManager;
    eManager = nullptr;
    logger->debug("End of osclooper main loop");
  }

  logger->debug("End of osclooper");
  return 0;
}
