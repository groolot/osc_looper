#include <osclooper/engineManager.hpp>
#include <iomanip>
#include <chrono>
#include <iostream>

using namespace std::literals::chrono_literals;

log4cxx::LoggerPtr engineManager::logger = log4cxx::Logger::getLogger("osclooper.engineManager");

engineManager *engineManager::singleton = nullptr;
std::map<std::string, engine *> engineManager::engines;

engineManager::engineManager()
{
  if(!log4cxx::LogManager::getLoggerRepository()->isConfigured())
  {
    log4cxx::BasicConfigurator::configure();
  }
}

engineManager::~engineManager()
{
  std::ostringstream output;

  for(auto engine : engines)
  {
    delete engine.second;
    engine.second = nullptr;
    output << "Engine "
           << std::quoted(engine.first)
           << " has been removed from the manager";
    logger->debug(output.str());
    output.str("");
  }

  engines.clear();
  singleton = nullptr;
  output << "engineManager destructed";
  logger->debug(output.str());
}

engineManager *engineManager::instance()
{
  if(singleton == nullptr)
  {
    singleton = new engineManager();
    logger->debug("Create new engineManager");
  }

  return singleton;
}

const std::map<std::string, engine *> *engineManager::get_engines()
{
  return &engines;
}

void engineManager::add(std::string name,
                        std::chrono::microseconds period,
                        unsigned short int osc_port)
{
  std::ostringstream output;

  if(engines.size() >= MAX_ENGINES)
  {
    output << "Too much engine created, max " << MAX_ENGINES;
    logger->error(output.str());
    throw std::length_error(output.str());
  }
  else if(engines.count(name) != 0)
  {
    output << "Engine name "
           << std::quoted(name)
           << " is already taken. NOT constructed!";
    logger->error(output.str());
    throw std::invalid_argument(output.str());
  }
  else
  {
    engines[name] = new engine(name, period, osc_port);
  }
}

void engineManager::list()
{
  std::ostringstream output;
  output << "Registered engines:" << std::endl;

  for(auto listed_engine : engines)
  {
    output << "  " << listed_engine.second->get_name() << std::endl;
  }

  std::cout << output.str();
}
