#include <osclooper/loopManager.hpp>
#include <osclooper/exceptions.hpp>

#include <iomanip>
#include <chrono>

using namespace std::literals::chrono_literals;

log4cxx::LoggerPtr loopManager::logger = log4cxx::Logger::getLogger("osclooper.loopManager");

loopManager::loopManager() :
  osc::Node("loopManager", this)
{
  if(!log4cxx::LogManager::getLoggerRepository()->isConfigured())
  {
    log4cxx::BasicConfigurator::configure();
  }
}

loopManager::~loopManager()
{
  std::ostringstream output;

  for(loop *loop_to_remove : loops)
  {
    output << "Loop "
           << std::quoted(loop_to_remove->get_name())
           << " has been removed from the manager";
    delete loop_to_remove;
    loop_to_remove = nullptr;
    logger->debug(output.str());
    output.str("");
  }

  loops.clear();
  output << "Destruct myself";
  logger->trace(output.str());
}

const std::vector<loop *> *loopManager::get_loops()
{
  return &loops;
}

void loopManager::add(std::chrono::microseconds /*period*/)
{
  throw NotImplemented();
}

void loopManager::list()
{
  throw NotImplemented();
  std::ostringstream output;
  output << std::endl << "Registered loops:";

  for(loop *listed_loop : loops)
  {
    output << std::endl << "   " << listed_loop->get_name();
  }

  logger->debug(output.str());
}
