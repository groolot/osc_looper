#include <osclooper/osc.hpp>
#include <osclooper/exceptions.hpp>

#include <stdexcept>
#include <iomanip>

log4cxx::LoggerPtr osc::Node::logger = log4cxx::Logger::getLogger("libosclooper.osc.Node");
log4cxx::LoggerPtr osc::RootNode::logger = log4cxx::Logger::getLogger("libosclooper.osc.RootNode");

/****************
 * Node
 ****************/
osc::Node::Node(std::string _name,
                osc::Node *_parent) :
  parent(_parent)
{
  if(!log4cxx::LogManager::getLoggerRepository()->isConfigured())
  {
    log4cxx::BasicConfigurator::configure();
  }
  set_name(_name);
  register_to_parent();
}

osc::Node::Node(std::string _name) : osc::Node(_name, nullptr) {}

osc::Node::~Node() {}

void osc::Node::set_name(std::string _name)
{
  if(_name.empty())
  {
    throw std::invalid_argument("osc::Node name cannot be an empty string");
  }
  else {
    unregister_methods();
    this->name = _name;
  }
}

std::string osc::Node::get_name() { return this->name; }

void osc::Node::set_parent(osc::Node *_parent)
{
  if(!parent)
  {
    parent = _parent;
    register_to_parent();
  }
  else {
    logger->warn(std::string("you try to override the parent of ")
                 + this->get_name()
      );
  }
}

osc::Node *osc::Node::get_parent() { return this->parent; }

osc::RootNode *osc::Node::get_root_parent()
{
  osc::Node *parent_cursor = this->parent;
  osc::Node *root_parent = this;
  while(parent_cursor)
  {
    root_parent = parent_cursor;
    parent_cursor = parent_cursor->parent;
  }
  return dynamic_cast<osc::RootNode *>(root_parent);
}

void osc::Node::register_to_parent()
{
  if(parent)
  {
    this->parent->get_children()->push_back(this);
  }
}

std::vector<osc::Node *> *osc::Node::get_children() { return &(this->children); }

std::string osc::Node::get_osc_path()
{
  std::string ancestor_path("");
  if(parent)
  {
    ancestor_path = parent->get_osc_path();
  }
  return ancestor_path + "/" + name;
}

int osc::Node::control_handler(const char *,
                               const char *,
                               lo_arg **,
                               int,
                               lo_message,
                               void *)
{
  std::string message("osc::Node::control_handler() cannot handle anything, you have to implement your own handler");
  logger->fatal(message);
  throw NotImplemented(message);
  return 0;
}

void osc::Node::add_osc_method(const std::string method_full_path,
                               const std::string types,
                               lo_method_handler _handler,
                               void *_data) {
  get_root_parent()->add_osc_method(method_full_path,
                                    types,
                                    _handler,
                                    _data);
}

void osc::Node::register_methods(lo_method_handler _handler, void *_caller)
{
  for(std::pair<std::string, std::string> method : control_methods)
  {
    // Since handler is a _static_ method, it does not know about the
    // current caller instance. So we MUST provide the caller pointer
    // as extra data to retrieve object inside the handler call.
    add_osc_method(this->get_osc_path() + method.first,
                   method.second,
                   _handler,
                   _caller);
  }
}

void osc::Node::unregister_methods()
{
  for(std::pair<std::string, std::string> method : control_methods)
  {
    std::ostringstream output;
    std::string method_full_path(this->get_osc_path() + method.first);
    output << get_name()
           << " - "
           << std::quoted(method_full_path);

    if(!method.second.empty())
    {
      output << " " << std::quoted(method.second);
    }

    get_root_parent()->del_method(this->get_osc_path() + method.first,
                                  method.second);
    output << " has been unregistered";
    logger->debug(output.str());
  }
}

/****************
 * RootNode
 ****************/
osc::RootNode::RootNode(std::string _name,
                        unsigned short int _osc_port) :
  osc::Node(_name),
  lo::ServerThread(_osc_port),
  osc_port(_osc_port)
{
  if(!log4cxx::LogManager::getLoggerRepository()->isConfigured())
  {
    log4cxx::BasicConfigurator::configure();
  }
  std::ostringstream output;

  if(!this->is_valid())
  {
    output << _name
           << " - cannot listen on port "
           << osc_port;
    logger->fatal(output.str());
    throw std::invalid_argument(output.str());
  }
  else {
    this->start();
    output << _name
           << " - now listening on port "
           << osc_port;
    logger->debug(output.str());
    output.str("");
    output.clear();
    output << "OSC server URL: " << get_root_parent()->url() << get_name();
    logger->info(output.str());
  }
  output.str("");
  output.clear();
}

osc::RootNode::~RootNode() {}

void osc::RootNode::add_osc_method(const std::string method_full_path,
                                   const std::string types,
                                   lo_method_handler _handler,
                                   void *_data) {
  std::ostringstream output;
  output << get_name()
         << " - "
         << std::quoted(method_full_path);

  if(!types.empty())
  {
    output << " " << std::quoted(types);
  }

  if(this->add_method(method_full_path, types, _handler, _data))
  {
    output << " has been registered";
    logger->debug(output.str());
  }
  else
  {
    output << " has NOT BEEN REGISTERED";
    throw std::runtime_error(output.str());
  }
}
