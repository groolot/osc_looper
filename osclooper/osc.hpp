#ifndef OSC_HPP
#define OSC_HPP

#ifdef HAVE_CONFIG_H
  #include "config.h"
#else
  #define OSC_PORT 9000
#endif

#include <lo/lo.h>
#include <lo/lo_cpp.h>

#include <string>
#include <vector>

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

namespace osc{


/******************************
 * osc::Node ******************
 *****************************/
class RootNode;
class Node
{
 public:
  explicit Node(std::string _name, Node *_parent);
  virtual ~Node();

  /** \brief The logger for the whole class
   *
   * It's using \e liblog4cxx for the logging mechanism.
   */
  static log4cxx::LoggerPtr logger;


  /** \brief Get node name
   *
   * \return the node name as \c std::string
   */
  std::string get_name();


  /** \brief Set parent node
   *
   * Set the parent node reference as pointer to \c osc::Node
   */
  void set_parent(osc::Node *_parent);


  /** \brief Get parent node
   *
   * \return the parent node reference as pointer to \c osc::Node
   */
  osc::Node *get_parent();


  /** \brief Get the root parent node
   *
   * \return the parent node reference as pointer to \c osc::RootNode
   */
  osc::RootNode *get_root_parent();


  /** \brief Register itself to its parent
   *
   * Create reverse association with its direct osc::Node parent,
   * which can be a osc::RootNode. This acknowledgement permits
   * descending walk.
   */
  virtual void register_to_parent() final;


  /** \brief Get children nodes
   *
   * \return the children nodes reference as \c std::vector of pointer
   * to \c osc::Node
   */
  std::vector<osc::Node *> *get_children();


  /** \brief Get full OSC path
   *
   * \return the full OSC path based on the inheritence
   */
  std::string get_osc_path();


  /** \brief A \c liblo callback function to receive notification of
   * matching message arriving in the server or server thread.
   *
   * This method has to be redefined in any inherited class. If not
   * this method will throw a NotImplemented exception.
   *
   * We cannot implement pure virtual, since the \c liblo library
   * await a static function pointer (because it's a C library), not a
   * pointer to member function. Help is welcomed to do that in C++.
   *
   * \param path \c char* The path that the incoming message was sent
   * to
   *
   * \param types \c char* If you specided types in your method
   * creation call then this will match those and the incoming types
   * will have been coerced to match, otherwise it will be the types
   * of the arguments of the incoming message
   *
   * \param arguments \c lo_arg** An array of \c lo_arg types
   * containing the values, e.g. if the first argument of the incoming
   * message is of type \c 'f' then the value will be found in \c
   * arguments[0]->f
   *
   * \param nbarguments \c int The number of arguments received
   *
   * \param message \c lo_message A structure containing the original
   * raw message as received. No type coercion will have occured and
   * the data will be in OSC byte order (bigendian)
   *
   * \param user_data \c void* This contains the \c user_data value
   * passed in the call to \c add_method, a \c engine* hopefully
   *
   * \return The return value tells the method dispatcher whether this
   * handler has dealt with the message correctly: a return value of 0
   * indicates that it has been handled, and it should not attempt to
   * pass it on to any other handlers, non-0 means that it has not
   * been handled and the dispatcher will attempt to find more
   * handlers that match the path and types of the incoming message.
   */
  static int control_handler(const char *path,
                             const char *types,
                             lo_arg **arguments,
                             int nbarguments,
                             lo_message message,
                             void *user_data);


  /** \brief \c osc::Node wrapper to \c osc::RootNode::add_method
   *
   * It calls the root parent node's \c add_osc_method() with provided
   * parameters.
   *
   * \sa osc::RootNode::add_osc_method()
   *
   * \param method_full_path the OSC command/method full path, as \c
   * std::string, that the handler has to handle
   *
   * \param types available argument types, as \c std::string, for this
   * command/method
   *
   * \param handler the static method callback that is able to process
   * the corresponding command/method
   *
   * \param data a value that will be passed to the callback function,
   * \c handler, when it's invoked matching from this command/method
   */
  virtual void add_osc_method(const std::string method_full_path,
                              const std::string types,
                              lo_method_handler handler,
                              void *data);


  /** \brief Initialize Node methods
   *
   * Define which OSC path is allowed inside the Node and map the
   * corresponding callback handler.
   *
   * \param handler the static method callback that is able to process
   * the methods
   *
   * \param caller pointer to the caller object, so the callback can
   * use object data
   */
  void register_methods(lo_method_handler handler, void *caller);


protected:
  Node(std::string _name);

  /** \brief Set Node name
   *
   * \param _name the node name as \c std::string
   */
  virtual void set_name(std::string _name);


  /** \brief OSC control methods
   *
   * These methods are defined with their arguments. \c std::multimap
   * is used to allow similar keys, expected different argument types.
   *
   * You can define as much exact method name as you want, with the
   * same type args or not. Be WARNED that two, or more, methods with
   * the same name and type args, the handler will be called as much
   * times as they are defined.
   */
  std::multimap<std::string, std::string> control_methods;


private:
  std::string name;
  Node *parent;
  std::vector<Node *> children;

  /** \brief Unregister all Node methods
   *
   * Unregister all OSC methods by path and type, from the parent
   * RootNode. Remove only those in \c control_methods, say from the
   * Node, not the entire tree.
   *
   * This is mainly needed when the Node name is changed. But it is
   * the inherited class responsability to call it.
   *
   */
  void unregister_methods();


};


/******************************
 * osc::RootNode **************
 *****************************/
class RootNode : public Node, public lo::ServerThread
{
public:
  /** \brief Constructor with name and OSC listening port
   */
  RootNode(std::string _name,
           unsigned short int _osc_port = OSC_PORT);


  ~RootNode();


  /** \brief The logger for the whole class
   *
   * It's using \e liblog4cxx for the logging mechanism.
   */
  static log4cxx::LoggerPtr logger;


  /** \brief Associate an OSC method path to callback
   *
   * It associates a \c method_full_path with authorized argument \c types
   * to be controlled through the corresponding \c handler.
   *
   * It prints some debug information and possibly throw an \c
   * std::runtime_error if the \c method_full_path cannot be registered to
   * the \c osc::RootNode::osc_server (probably because it already
   * exists).
   *
   * \param method_full_path the OSC command/method full path, as \c
   * std::string, that the handler is able to handle ; the full path
   * is computed from osc::Node inheritance
   *
   * \param types available argument types, as \c std::string, for this
   * command/method
   *
   * \param handler the static method callback that is able to process
   * the corresponding command/method
   *
   * \param data a value that will be passed to the callback function,
   * \c handler, when it's invoked matching from this command/method
   */
  void add_osc_method(const std::string method_full_path,
                      const std::string types,
                      lo_method_handler handler,
                      void *data) override final;

protected:
  /** \brief OSC port number
   */
  unsigned short int osc_port;
};

}

#endif  // OSC_HPP
