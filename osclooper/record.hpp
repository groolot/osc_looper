#ifndef RECORD_HPP
#define RECORD_HPP

#include <lo/lo.h>
#include <lo/lo_cpp.h>

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

class record
{
public:
  explicit record(lo::Message, lo::Address);
  ~record();

  /** \brief The logger for the whole class
   *
   * It's using \e liblog4cxx for the logging mechanism.
   */
  static log4cxx::LoggerPtr logger;

  /** \brief Encapsulated liblo Message */
  lo::Message message;

  /** \brief Encapsulated liblo Address */
  lo::Address address;
};

#endif // RECORD_HPP
