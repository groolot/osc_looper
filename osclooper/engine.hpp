#ifndef ENGINE_HPP
#define ENGINE_HPP

#ifdef HAVE_CONFIG_H
  #include "config.h"
#else
  #define OSC_PORT 9000
  #define MAX_ENGINES 10
#endif

#include <osclooper/loopManager.hpp>
#include <osclooper/osc.hpp>

#include <chrono>
#include <string>
#include <map>

#include <thread>
#include <mutex>
#include <condition_variable>

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

using namespace std::literals::chrono_literals;

class engineManager;

class engine : public osc::RootNode
{
 public:
  /** \brief Constructor with name, refresh period and OSC port
   *
   * This is to construct an engine with a given name, a refresh
   * period time duration and an OSC port to reach it. The name is
   * used to identify the engine inside program and in OSC control
   * path. The period is used to define the time duration consuming
   * between each cycle.
   *
   * \param name \c std::string Name given to the engine
   *
   * \param period \c std::chrono::duration Time duration. Use \c
   * std::literals::chrono_literals to help defining time at several
   * unities ; if not using literals, time is specified in
   * microseconds.
   *
   * \param osc_port \c unsigned \c short \c int OSC server port
   * number, default to port 9000
   */
  explicit engine(std::string name = "noName",
                  std::chrono::microseconds period = 1s,
                  unsigned short int osc_port = OSC_PORT);

  /** \brief Destruct engine object
   *
   * Stop the running thread. Remove the engine from the manager, then
   * destruct.
   */
  ~engine();

  /** \brief Start the engine's thread
   */
  void start();

  /** \brief Stop the running engine's thread
   */
  void stop();

  /** \brief Set engine name
   *
   * \param _name Engine's identifier name as \c std::string
   */
  void set_name(std::string _name) override;

  /** \brief Period time accessor
   *
   * \return Engine's time period \c _period as \c
   * std::chrono::microseconds
   */
  std::chrono::microseconds get_period();

  /** \brief Loop manager accessor
   *
   * \return Loop manager pointer as \c loopManager*
   */
  loopManager *get_loop_manager();

  /** \brief OSC server port accessor
   */
  unsigned short int get_osc_port();

  /** \brief Is engine thread running?
   *
   * \return bool True if the engine is computing the thread loop,
   * false instead
   */
  bool is_running();

  /** \brief The logger for the whole class
   *
   * It's using \e liblog4cxx for the logging mechanism.
   */
  static log4cxx::LoggerPtr logger;

  /** \brief Class scope mutex, helping thread synchronisation
   *
   * The aim of this mutex is to permit locking mechanism within the
   * engine class. One instance can catch the mutex to mmodify the \c
   * std::condition_variable element.
   */
  static std::mutex latch;

  /** \brief Class scope condition_variable
   *
   * This is the \c std::condition_variable controling the threads
   * continuity
   */
  static std::condition_variable condition;

 private:
  /** \brief Loop manager
   *
   * To control loops inside the engine
   */
  loopManager *loop_manager;

  /** \brief Engine period granularity
   *
   * Engine period granularity, stored in microseconds
   */
  std::chrono::microseconds period;

  /** \brief Engine thread switch
   *
   * This switch is used to exit the main thread loop. By the way, the
   * engine is not stopped at all, nor destructed. The thread can be
   * restarted with the \c condition_variable mechanism
   */
  bool running_thread;

  /** \brief Engine thread representation
   */
  std::thread thread;

  /** \brief Thread frontend process
   */
  void run();

  /** \brief Thread backend process
   */
  void execute_cycle();

  /** \brief \c engine implementation of the \c
   * osc::Node::control_handler()
   *
   * A \c liblo callback function to receive notification of matching
   * message arriving in the server or server thread.
   *
   * \param path \c char* The path that the incoming message was sent
   * to
   *
   * \param types \c char* If you specided types in your method
   * creation call then this will match those and the incoming types
   * will have been coerced to match, otherwise it will be the types
   * of the arguments of the incoming message
   *
   * \param arguments \c lo_arg** An array of \c lo_arg types
   * containing the values, e.g. if the first argument of the incoming
   * message is of type \c 'f' then the value will be found in \c
   * arguments[0]->f
   *
   * \param nbarguments \c int The number of arguments received
   *
   * \param message \c lo_message A structure containing the original
   * raw message as received. No type coercion will have occured and
   * the data will be in OSC byte order (bigendian)
   *
   * \param user_data \c void* This contains the \c user_data value
   * passed in the call to \c add_method, a \c engine* hopefully
   *
   * \return The return value tells the method dispatcher whether this
   * handler has dealt with the message correctly: a return value of 0
   * indicates that it has been handled, and it should not attempt to
   * pass it on to any other handlers, non-0 means that it has not
   * been handled and the dispatcher will attempt to find more
   * handlers that match the path and types of the incoming message.
   */
  static int control_handler(const char *path,
                             const char *types,
                             lo_arg **arguments,
                             int nbarguments,
                             lo_message message,
                             void *user_data);

  /** \brief Validation
   *
   * \param returl \c std::string Reference to the provided string URL
   */
  static void validate_returl(std::string &returl);
};

#endif // ENGINE_HPP
